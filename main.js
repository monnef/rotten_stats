const puppeteer = require('puppeteer');
const args = require('yargs').help(false).argv;
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const moment = require('moment');

const crash = (msg) => { throw new Error(msg); };

const verbose = args.v || args.verbose;
const vLog = verbose ? (...args) => console.log('[rotten_stats] ' + moment().format('YYYY/MM/DD HH:mm:ss.SSS:'), ...args) : () => {};
vLog('args: ', args);
const debug = args.d || args.debug;

const getInnerTextFromEl = el => el.innerText;
const getText = (page) => async (sel) => {
  return await page.$eval(sel, getInnerTextFromEl) || null;
};

const getInnerHTMLFromEl = el => el.innerHTML;
const getInnerHTML = (page) => async (sel) => {
  return await page.$eval(sel, getInnerHTMLFromEl) || null;
};

const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

const getRandomArbitrary = (min, max) => Math.random() * (max - min) + min;

const getData = async (opts) => {
  const {slug, gotoTimeout} = opts;
  vLog('getData', opts);

  const timeout = gotoTimeout || (10 * 1000);
  const basePupOptions = {
    ignoreHTTPSErrors: true,
    timeout,
    defaultViewport: {
      width: 1366 - 6,
      height: 768 - 64
    }
  };
  const debugPupOptions = {
    headless: false,
    slowMo: 100
  };
  const pupOptions = debug ? {...basePupOptions, ...debugPupOptions} : basePupOptions;
  const browser = await puppeteer.launch(pupOptions);
  const page = await browser.newPage();
  try {
    await page.goto('https://www.rottentomatoes.com/' + slug, {timeout});
  } catch (e) {
    if (e.name === 'TimeoutError') {
      vLog('goto failed: probably RT bug (endless loading), ignoring');
    } else {
      console.error('goto failed:', e.message, JSON.stringify(e, null, 2));
      throw e;
    }
  }
  await page.screenshot({path: 'screenshot.png'});
  const getTextP = getText(page);
  const getHTMLP = getInnerHTML(page);

  const shortSleep = () => sleep(getRandomArbitrary(100, 1000));
  const longSleep = () => sleep(getRandomArbitrary(1000, 3000));

  await shortSleep();

  vLog('getting Verified audience meter');
  const AudienceScoreSel = '.mop-ratings-wrap__half:nth-child(2) .mop-ratings-wrap__score .mop-ratings-wrap__percentage';
  const AudienceScoreTvSel = '.audience-score .meter-value span';
  const hasAudienceMeter = (await page.$$(AudienceScoreSel)).length > 0;
  const hasAudienceTvMeter = (await page.$$(AudienceScoreTvSel)).length > 0;
  const audienceMeterRaw = await (async () => {
    if (hasAudienceMeter) return await getTextP(AudienceScoreSel);
    if (hasAudienceTvMeter) return await getTextP(AudienceScoreTvSel);
    return null;
  })();
  const verifiedAudienceMeter = audienceMeterRaw ? Number.parseInt(audienceMeterRaw.replace('%', ''), 10) : null;

  vLog('getting Verified audience ratings count');
  const VerifiedAudienceRatingsCountSel = '.audience-score .mop-ratings-wrap__review-totals strong.mop-ratings-wrap__text--small';
  const VerifiedAudienceRatingsCountTvSel = '.audience-info > div:nth-child(2)';
  const hasVerifiedAudienceMovieRatingsCount = (await page.$$(VerifiedAudienceRatingsCountSel)).length > 0;
  const hasVerifiedAudienceTvRatingsCount = (await page.$$(VerifiedAudienceRatingsCountTvSel)).length > 0;
  const verifiedAudienceRatingsCountRaw = await (async () => {
    if (hasVerifiedAudienceMovieRatingsCount) return await getTextP(VerifiedAudienceRatingsCountSel);
    if (hasVerifiedAudienceTvRatingsCount) return await getTextP(VerifiedAudienceRatingsCountTvSel);
    return null;
  })();
  const verifiedAudienceRatingsCount = (() => {
    if (verifiedAudienceRatingsCountRaw === 'N/A') return null;
    const prepared = verifiedAudienceRatingsCountRaw.replace('Verified Ratings: ', '').replace('User Ratings: ', '').replace(/,/g, '');
    return Number.parseInt(prepared, 10);
  })();

  vLog('opening overlay with details');
  const OverlayButtonSel = '#scoreModal_showScoreboardBtn';
  if ((await page.$$(OverlayButtonSel)).length > 0) await page.click(OverlayButtonSel);

  await longSleep();

  vLog('getting Critic meter');
  const CriticMeterSel = '.mop-ratings-wrap__half:nth-child(1) .mop-ratings-wrap__score .mop-ratings-wrap__percentage';
  const CriticMeterTvSel = '.critic-score .meter-value span';
  const hasCriticMeter = await page.$(CriticMeterSel) !== null;
  const hasCriticTvMeter = await page.$(CriticMeterTvSel) !== null;
  const criticMeterRaw = await (async () => {
    if (hasCriticMeter) return await getTextP(CriticMeterSel);
    if (hasCriticTvMeter) return await getTextP(CriticMeterTvSel);
    return null;
  })();
  const criticMeter = criticMeterRaw ? Number.parseInt(criticMeterRaw, 10) : null;

  vLog('getting Critic rating');
  const CriticRatingSel = '#js-tomatometer-overlay .mop-ratings-wrap__more-overlay_left > ul.score-modal__details-wrap li:first-child > strong';
  const CriticRatingTvSel = '#scoreStats > div:first-child';
  const hasCriticRating = await page.$(CriticRatingSel) !== null;
  const hasCriticTvRating = await page.$(CriticMeterTvSel) !== null;
  const criticRatingRaw = await (async () => {
    if (hasCriticRating) return await getTextP(CriticRatingSel);
    if (hasCriticTvRating) return await getTextP(CriticRatingTvSel);
    return null;
  })();
  const criticRating = criticRatingRaw ? Number.parseFloat(criticRatingRaw.replace('Average Rating: ', '').replace(/\/.*$/)) : null;

  const extractAudienceRatingFromDetails = async () => {
    const audienceRatingSel = '#js-tomatometer-overlay .mop-ratings-wrap__more-overlay_right .score-modal__details-wrap > li.score-modal__text--audience-average-rating > strong';
    const audienceRatingTvSel = '.audience-info > div:nth-child(1)';
    const hasAudienceMovieRating = (await page.$$(audienceRatingSel)).length >= 1;
    const hasAudienceTvRating = (await page.$$(audienceRatingTvSel)).length >= 1;
    const audienceRatingRaw = await (async () => {
      if (hasAudienceMovieRating) return await getHTMLP(audienceRatingSel);
      if (hasAudienceTvRating) return await getTextP(audienceRatingTvSel);
      return null;
    })();
    vLog({
      hasAudienceMovieRating,
      hasAudienceTvRating,
      audienceRatingRaw,
    });
    return audienceRatingRaw ? Number.parseFloat(audienceRatingRaw.replace('Average Rating: ', '').replace(/\/.*$/, '')) : null;
  };

  vLog('getting Verified audience rating');
  const verifiedAudienceRating = await extractAudienceRatingFromDetails();

  vLog('opening all audience tab');
  const AllAudienceButtonSel = '.js-audience-toggle-btn[data-type="all"]';
  const allAudienceButtonExists = (await page.$$(AllAudienceButtonSel)).length > 0;
  if (allAudienceButtonExists) await page.click(AllAudienceButtonSel);

  await longSleep();

  vLog('getting All audience rating');
  const allAudienceRating = allAudienceButtonExists ? await extractAudienceRatingFromDetails() : null;

  const extractAudienceMeterFromDetails = async () => {
    const audienceMeterSel = '.mop-ratings-wrap__more-overlay_right .score-modal__percent';
    const audienceMeterRaw = await getTextP(audienceMeterSel);
    return Number.parseFloat(audienceMeterRaw);
  };

  vLog('getting All audience meter');
  const allAudienceMeter = allAudienceButtonExists ? await extractAudienceMeterFromDetails() : null;

  const extractAudienceRatingsCount = async () => {
    const audienceRatingsCountSel = '.mop-ratings-wrap__more-overlay_right ul.score-modal__details-wrap > li:not(.score-modal__text--audience-average-rating):not(.score-modal__dets) strong';
    const audienceRatingsCountRaw = await getTextP(audienceRatingsCountSel);
    return Number.parseFloat(audienceRatingsCountRaw.replace(',',''));
  };

  vLog('getting All audience ratings count');
  const allAudienceRatingsCount = allAudienceButtonExists ? await extractAudienceRatingsCount() : null;

  vLog({
    verifiedAudienceMeter,
    hasAudienceMeter,
    verifiedAudienceRatingsCountRaw,
    verifiedAudienceRatingsCount,
    verifiedAudienceRating,
    criticMeterRaw,
    criticMeter,
    hasCriticRating,
    criticRatingRaw,
    criticRating,
  });

  await browser.close();

  return {
    date: new Date(),
    slug,
    audience: {
      verified: {
        meter: verifiedAudienceMeter,
        ratingsCount: verifiedAudienceRatingsCount,
        rating: verifiedAudienceRating
      },
      all: {
        meter: allAudienceMeter,
        ratingsCount: allAudienceRatingsCount,
        rating: allAudienceRating
      }
    },
    critic: {
      meter: criticMeter,
      rating: criticRating
    }
  };
};

const saveToDb = async ({data, dbFile}) => {
  vLog('saveToDb', {dbFile});

  const adapter = new FileSync(dbFile);
  const db = low(adapter);
  db.defaults({}).write();

  const itemKey = [data.slug];

  if (!db.get(itemKey).value()) db.set(itemKey, []).write();

  db.get(itemKey)
    .push(data)
    .write();
};

const printHelp = () => {
  console.log('rotten_stats - obtain (and optionally save) meters and scores from Rotten Tomatoes');
  console.log();
  console.log('  --timeout <time> | -t <time> : set timeout in milliseconds');
  console.log('  --slug <slug> : part of URL after domain, slash can be omitted : --slug m/matrix');
  console.log('  --save | -s | -S : save to DB');
  console.log('  --db <fileName> : set DB file, defaults to \'db.json\' : --db my_db.json');
  console.log('  -q | --quiet : suppress result output');
  console.log('  -v | --verbose : extra logging');
  console.log('  -d | --debug');
};

const main = async () => {
  const slug = args.slug ? args.slug.replace(/^\//, '') : null;
  const gotoTimeout = args.timeout || args.t;
  if (args.slug) {
    const data = await getData({
      slug,
      verbose,
      gotoTimeout
    });
    if (!args.q && !args.quiet) console.log(JSON.stringify(data, null, 2));
    if (args.save || args.s || args.S) {
      const dbFile = args.db || 'db.json';
      await saveToDb({data, dbFile, verbose});
    }
  } else {
    printHelp();
    if (!args.help && !args.h) process.exit(1);
  }
};

main();
