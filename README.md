rotten_stats
===

This is a small tool for getting rating related data of movies and TV shows from Rotten Tomatoes site.

Requirements
===
* [NodeJS](https://nodejs.org) (tested with 10.16.3)

Installation
===

0\. Install Node
---
See [Requirements](#requirements).

1\. Download sources
---
```sh
$ git clone https://gitlab.com/monnef/rotten_stats.git
$ cd rotten_stats
```

2\. Install npm dependencies
---
```sh
$ npm i
```

Usage
===

Help
---
```sh
$ node main.js
rotten_stats - obtain (and optionally save) meters and scores from Rotten Tomatoes

  --timeout <time> | -t <time> : set timeout in milliseconds
  --slug <slug> : part of URL after domain, slash can be omitted : --slug m/matrix
  --save | -s | -S : save to DB
  --db <fileName> : set DB file, defaults to 'db.json' : --db my_db.json
  -q | --quiet : suppress result output
  -v | --verbose : extra logging
  -d | --debug
```

Example
---
```sh
$ node main.js --slug m/star_wars_the_rise_of_skywalker
```

output (stdout):
```json
{
  "date": "2019-12-24T13:10:55.431Z",
  "slug": "m/star_wars_the_rise_of_skywalker",
  "audience": {
    "verified": {
      "meter": 86,
      "ratingsCount": 43394,
      "rating": 4.32
    },
    "all": {
      "meter": 77,
      "ratingsCount": 98944,
      "rating": 3.96
    }
  },
  "critic": {
    "meter": 55,
    "rating": 6.21
  }
}
```

You can also save results to a JSON database file (individual results will be appended to matching array):

```sh
$ node main.js --slug m/matrix --save --db test.json -t 1500 --quiet
$ cat test.json
```
```json
{
  "m/matrix": [
    {
      "date": "2019-12-24T12:19:53.772Z",
      "slug": "m/matrix",
      "audience": {
        "verified": {
          "meter": 85,
          "ratingsCount": 33324202,
          "rating": 3.64
        },
        "all": {
          "meter": null,
          "ratingsCount": null,
          "rating": null
        }
      },
      "critic": {
        "meter": 88,
        "rating": 7.68
      }
    }
  ]
}
```

Notes
===
Script's behaviour is possible to detect (via JS on page). If you don't want to risk anything, it is advisable to use VPS and/or VPN and similar.

License
===
GPLv3
